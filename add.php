<?php

$_POST || header("Location: /");

if(!$_POST['task']){
    header("Location: /");
    die();
}

$task = $_POST['task'];

require 'db-connect.php';

$res = $pdo->prepare("INSERT into tasks (name) VALUES (?)");
$res->execute([$task]);

header("Location: /");