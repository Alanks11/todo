<?php

$_POST || header("Location: /");

require "db-connect.php";

$res = $pdo->prepare("DELETE FROM `tasks` WHERE `id` = ?;");
$res->execute([$_POST['id']]);

header("Location: /");