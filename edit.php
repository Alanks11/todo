<?php
$_POST || header("Location: /");

require "db-connect.php";

$res = $pdo->prepare("UPDATE `tasks` SET `name` = ? WHERE `id` = ?;");
$res->execute([$_POST['name'], $_POST['id']]);

header("Location: /");