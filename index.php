<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Список дел</title>

    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="container">
        <h1>Список дел</h1>
        <form action="/add.php" method="POST" class="input">
            <input type="text" name="task" id="task" placeholder="Нужно сделать..." class="form-control"
                maxlength="255">
            <button type="submit" name="sendTask">+</button>
        </form>
        <?php
        
        require 'db-connect.php';

        $res = $pdo->query("SELECT * FROM tasks ORDER BY id DESC");
      
        echo "<ul>";
        foreach($res as $r){
            echo "<li>
            <p>{$r['name']}</p>
            <div>
            <form action='/delete.php' method='POST'>           
            <input type='hidden' name='id' value='{$r['id']}'>
            <button type='submit'>Удалить</button>
            </form>
            
            </div>
            </li>";
        }
        echo "</ul>";
        
        ?>


    </div>


    <script src="js/script.js"></script>
</body>

</html>