
let items = document.querySelectorAll('li p');

items.forEach(el => {
    el.addEventListener('click', itemEdit);
})

function itemEdit() {
    let text = this.textContent;
    let id = this.parentNode.querySelector(`input`).getAttribute('value');
    this.parentNode.innerHTML = `
        <form action='/edit.php' method='POST'>
        <input type="text" name="name" value="${text}" maxlength="255">
        <input type='hidden' name='id' value='${id}'>
        <button type='submit'>Изменить</button>
        </form>
    `;
}

